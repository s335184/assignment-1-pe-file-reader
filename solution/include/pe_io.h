#ifndef SECTION_EXTRACTOR_PE_IO_H
#define SECTION_EXTRACTOR_PE_IO_H

#include "pe_file.h"

enum read_status{
    READ_OK = 0,
    READ_MS_SKIP_ERROR,
    READ_OFFSET_ERROR,
    READ_SIGNATURE_ERROR,
    READ_HEADER_ERROR,
    READ_SECTION_HEADER_ERROR,
    READ_OPTIONAL_HEADER_SIZE_ERROR,
    READ_MEMORY_ALLOCATION_ERROR
};

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_NOT_FOUND_ERROR,
    WRITE_MEMORY_ALLOCATION_ERROR
};

int read_pe_file(FILE *in, struct PEFile *file);
int write_pe_section(FILE *in, FILE *out, struct PEFile *file, char *name);

#endif //SECTION_EXTRACTOR_PE_IO_H
