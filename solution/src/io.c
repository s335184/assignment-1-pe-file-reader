#include "io.h"
/// Application name string
#define APP_NAME "section-extractor"

/// @brief Prints the application's arguments' format.
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f){
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}
