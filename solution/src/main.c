/// @file 
/// @brief Main application file

#include "io.h"
#include "pe_io.h"


/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv){
    if(argc < 4){
        usage(stdout);
        return 1;
    }
    ///Main PE file structure
    struct PEFile *peFile = malloc(sizeof(struct PEFile));
    if(!peFile){
        printf("PE File memory allocation error!");
        free(peFile);
        return 1;
    }
    ///Input file
    FILE *in = fopen(argv[1], "rb");
    ///Name of section
    char *name = argv[2];
    ///Output file
    FILE *out = fopen(argv[3], "wb");

    int read_status = read_pe_file(in, peFile);
    printf("%d", read_status);
    int write_status = write_pe_section(in, out,peFile, name);
    printf("%d", write_status);

    free(peFile->sectionHeaders);
    free(peFile);
    fclose(in);
    fclose(out);
    return 0;
}
