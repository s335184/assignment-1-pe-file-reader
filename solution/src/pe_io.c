#include "pe_io.h"
#define HEADER_NAME_SIZE 8
/**
 * @brief Reading data from PE file
 * @param in Input file
 * @param file Structure of PE file
 */
int read_pe_file(FILE *in, struct PEFile *file) {
    //Skipping MS-DOS
    if(fseek(in, 0x3c, SEEK_SET) != 0){
        return READ_MS_SKIP_ERROR;
    }
    //Offset - contains the offset of PE file
    uint32_t offset;
    if(fread(&offset, sizeof(offset), 1, in) != 1){
        return READ_OFFSET_ERROR;
    }
    if(fseek(in, (long)offset, SEEK_SET != 0)){
        return READ_OFFSET_ERROR;
    }
    //Signature - PE\x0\x0
    if(fread(&file->signature, sizeof(file->signature), 1, in) != 1){
        return READ_SIGNATURE_ERROR;
    }
    if(fread(&file->header, sizeof(file->header), 1, in) != 1){
        return READ_HEADER_ERROR;
    }

    file->sectionHeaders = (malloc(file->header.NumberOfSections * sizeof(struct SectionHeader)));
    if(!file->sectionHeaders){
        return READ_MEMORY_ALLOCATION_ERROR;
    }
    if(fseek(in, file->header.SizeOfOptionalHeader, SEEK_CUR) != 0){
        return READ_OPTIONAL_HEADER_SIZE_ERROR;
    }

    if(fread(file->sectionHeaders, sizeof(struct SectionHeader), file->header.NumberOfSections, in) != 1){
        return READ_SECTION_HEADER_ERROR;
    }
    return READ_OK;
}

/**
 * @brief Outputs the PE section information into the input file.
 * @param in Input file
 * @param out Output file
 * @param peFile Structure of PE file
 * @param name Name of section
 */
int write_pe_section(FILE *in, FILE *out, struct PEFile *peFile, char *name) {
    for (int16_t i = 0; i < peFile->header.NumberOfSections; i++) {
        if (strncmp((char*)peFile->sectionHeaders[i].name, name, HEADER_NAME_SIZE) == 0) {

            struct SectionHeader section_header = peFile->sectionHeaders[i];
            fseek(in, (long) section_header.PointerToRawData, SEEK_SET);
            char *section = (malloc(section_header.SizeOfRawData));
            if(!section){
                free(section);
                return WRITE_MEMORY_ALLOCATION_ERROR;
            }

            if(fread(section, section_header.SizeOfRawData, 1, in) != 1){
                return WRITE_ERROR;
            }
            fwrite(section, section_header.SizeOfRawData, 1, out);

            free(section);
            return WRITE_OK;
        }
    }
    return WRITE_NOT_FOUND_ERROR;
}
